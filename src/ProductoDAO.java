
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Alfonso
 */
public class ProductoDAO {

   private List<Producto> LstProducto;
   private Producto producto;
   private ArchivoLE Ale;

   public ProductoDAO() {
      LstProducto = new ArrayList<>();
      producto = new Producto();
      Ale = new ArchivoLE("Producto.txt");
   }
   
   public void agregarRegistro(Producto P) throws IOException{
      LstProducto.add(P);
      Ale.escribirArchivo(P.getNumProducto() + ", " + P.getDescripcion());
   }
   
   public void eliminarRegistro(Producto P){
      LstProducto.remove(buscarProducto(P));
   }
   
   public List<Producto> listarRegistros() {
      return LstProducto;
   }
   
   private int buscarProducto(Producto P){
      return LstProducto.indexOf(P);
   }
   
}
