
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

public class ArchivoLE {

  // Referencia a archivo
  private File f;

// Escritura
  private FileWriter fw;
  private BufferedWriter bw;

  private FileReader fr;
  private BufferedReader br;

  private ArrayList listaRegistros;

  public ArchivoLE(String NombreArchivo) {
    f = new File(NombreArchivo);
    listaRegistros = new ArrayList();
  }

  public void escribirArchivo(String Dato) throws IOException {

    fw = new FileWriter(f, true);
    bw = new BufferedWriter(fw);
    bw.write(Dato + "\n");
    bw.close();
    fw.close();
  }

  public ArrayList leerArchivo() throws FileNotFoundException, IOException {

    String lineaLeida;
    
    fr = new FileReader(f);
    br = new BufferedReader(fr);

    while ((lineaLeida = br.readLine()) != null) {
      listaRegistros.add(lineaLeida);

    }

    return listaRegistros;
  }

}
