/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Alfonso
 */
public class Producto {

   private String NumProducto;
   private String Descripcion;

   public Producto() {
   }

   public String getNumProducto() {
      return NumProducto;
   }

   public void setNumProducto(String NumProducto) {
      this.NumProducto = NumProducto;
   }

   public String getDescripcion() {
      return Descripcion;
   }

   public void setDescripcion(String Descripcion) {
      this.Descripcion = Descripcion;
   }
   
}
